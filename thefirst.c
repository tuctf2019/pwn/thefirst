#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

// ret2win
void printFlag() {
	system("/bin/cat ./flag.txt");
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	printf("Let's see what you can do\n> ");
	char buf[16];
	// gets()... classic
	gets(buf);

    return 0;
}
