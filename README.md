# thefirst

Desc: `Get warmed up, we'll be here for a while.`

Architecture: x86

Given files:

* thefirst

Hints:

* Notice any interesting functions lying around?

Flag: `TUCTF{0n3_d0wn..._50_m4ny_70_60}`
